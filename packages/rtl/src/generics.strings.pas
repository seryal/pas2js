{
    This file is part of the Pas2JS run time library.
    Copyright (c) 2023 by Michael Van Canneyt
      
    Delphi-compatible generics string constansts
    
    See the file COPYING.FPC, included in this distribution,
    for details about the copyright.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

 **********************************************************************}
{$IFNDEF FPC_DOTTEDUNITS}
unit Generics.Strings;
{$ENDIF}

{$Mode Delphi}

interface

resourcestring
  SArgumentOutOfRange = 'Argument out of range';
  //SArgumentNilNode = 'Node is nil';
  //SDuplicatesNotAllowed = 'Duplicates not allowed in dictionary';
  //SCollectionInconsistency = 'Collection inconsistency';
  //SCollectionDuplicate = 'Collection does not allow duplicates';
  //SDictionaryKeyDoesNotExist = 'Dictionary key does not exist';
  //SItemNotFound = 'Item not found';

implementation

end.

