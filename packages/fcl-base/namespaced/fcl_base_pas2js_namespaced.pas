{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit fcl_base_pas2js_namespaced;

{$warn 5023 off : no warning about unused units}
interface

uses
  Fcl.CustApp, Fcl.App.NodeJs, Fcl.App.Browser, Fcl.Expressions, Fcl.App.ServiceWorker, Pascal.CodeGenerator;

implementation

end.
