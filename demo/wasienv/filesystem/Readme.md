# WebAssembly Filesystem demo

The filesystem demo relies on ZenFS:

https://zen-fs.github.io/

Included here are 2 files from ZenFS:

browser.min.js (ZenFS core)
browser.dom.js (ZenFS DOM backends)

They must be included in any pas2js program if you wish to enable filesystem
support for WebAssembly.




